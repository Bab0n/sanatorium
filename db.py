from sqlalchemy import create_engine, Table, Column, Integer, MetaData, String


meta = MetaData()

staff = Table('Staff', meta, 
    Column('id', Integer, primary_key=True),
    Column('name', String, nullable=False),
    Column('surname', String, nullable=False),
    Column('position', String, nullable=False),
    Column('salary', Integer, nullable=False),
    )
services = Table('Services', meta,
    Column('id', Integer, primary_key=True),
    Column('title', String, nullable=False),
    Column('price', Integer, nullable=False),
    )

if __name__ == '__main__':
    engine = create_engine('sqlite:///sanatorium.db')
    meta.create_all(engine)