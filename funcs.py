from contextlib import contextmanager
from sqlalchemy import MetaData, Table, create_engine
from sqlalchemy.orm import mapper, sessionmaker


class Staff():
    def __init__(self, id, name, surname, position, salary):
        self.id = id
        self.name = name
        self.surname = surname
        self.position = position
        self.salary = salary


class Service():
    def __init__(self, id, title, price):
        self.id = id
        self.title = title
        self.price = price



engine = create_engine('sqlite:///sanatorium.db')
meta = MetaData(engine)
staff_t = Table('Staff', meta, autoload=True)
mapper(Staff, staff_t)
services_t = Table('Services', meta, autoload=True)
mapper(Service, services_t)
Session = sessionmaker(bind=engine)

@contextmanager
def session():
    new_session = Session()
    try:
        yield new_session
        new_session.commit()
    except Exception:
        new_session.rollback()
    finally:
        new_session.close()


def add_staff(name, surname, position, salary):
    with session() as s:
        try: 
            id = max(i.id for i in s.query(Staff))+1
        except Exception:
            id = 0
        if id not in [j.id for j in s.query(Staff)]:
            new_staff = Staff(id, name, surname, position, salary)
            s.add(new_staff)
            return True
        else:
            return False


def remove_staff(id):
    with session() as s:
        staff = [j for j in s.query(Staff) if j.id == id]
        if len(staff)>0:
            employee = staff[0]
            s.delete(employee)
            return True
        else:
            return False

def add_service(title, price):
    with session() as s:
        ids = [i.id for i in s.query(Service)]
        try: 
            id = max(ids)+1
        except Exception:
            id = 0
        if id not in ids:
            new_service = Service(id, title, price)
            s.add(new_service)
            return True
        else:
            return False


def remove_service(id):
    with session() as s:
        services = [j for j in s.query(Service) if j.id == id]
        if len(services)>0:
            serv = services[0]
            s.delete(serv)
            return True
        else:
            return False

def get_staff():
    ret = []
    with session() as s:
        stff = [i for i in s.query(Staff)]
        for i in stff:
            ret.append([i.id, i.name, i.surname, i.position, i.salary])
        return ret


def get_services():
    ret = []
    with session() as s:
        serv = [i for i in s.query(Service)]
        for i in serv:
            ret.append([i.id, i.title, i.price])
        return ret


if __name__ == '__main__':
    add_staff('Александ', 'Соколов', 'Заведующий', 35000)