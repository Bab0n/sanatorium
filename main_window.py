import sys
import funcs
from windows import *
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLineEdit, QMainWindow, QDialog, QLabel, QTableWidget, QTableWidgetItem


def button(text, wnd, func, pos, w, h):
        btn = QPushButton(text, wnd)
        btn.clicked.connect(func)
        btn.resize(w, h)
        btn.move(pos[0], pos[1])
        btn.setStyleSheet("""
            color: black;
            text-align: center;
            text-decoration: none;
            font-size: 12px;""")
        return btn


class Window(QWidget):
    title = "Sanatorium [Staff]"
    wind_show = 1
    
    
    def __init__(self):
        super().__init__()
        self.initUI()
        self.title = "Sanatorium [Staff]"


    def initUI(self):
        self.window = QMainWindow()
        self.window.setGeometry(300, 300, 500, 300)
        self.window.setWindowTitle(self.title)

        button("Swap window", self.window, self.swap, [110, 205], 100, 50)
        button("UPDATE", self.window, self.upd, [210, 205], 100, 50)

        if self.wind_show == 1:
            self.staff_table()
            button("New staff", self.window, self.add_staff, [10, 205], 100, 25)
            button("Remove staff", self.window, self.remove_staff, [10, 230], 100, 25)
        
        if self.wind_show == 0:
            self.service_table()
            button("Add service", self.window, self.add_service, [10, 205], 100, 25)
            button("Remove service", self.window, self.remove_service, [10, 230], 100, 25)
        self.window.show()
    

    def upd(self):
        self.initUI()


    def service_table(self):
        table = QTableWidget(self.window)
        service = funcs.get_services()
        table.setColumnCount(3)
        table.setRowCount(len(service))
        table.setHorizontalHeaderLabels(["ID", "TITLE", "PRICE"])
        for row, elem in enumerate(service):
            for col, txt in enumerate(elem):
                table.setItem(row, col, QTableWidgetItem(str(txt)))
        table.verticalHeader().hide()
        table.move(50, 10)
        table.resize(400, 180)
        table.setColumnWidth(0, 1)
        table.setColumnWidth(1, 274)
        table.setColumnWidth(2, 85)


    def staff_table(self):
        table = QTableWidget(self.window)
        staff = funcs.get_staff()
        table.setColumnCount(5)
        table.setRowCount(len(staff))
        table.setHorizontalHeaderLabels(["ID", "NAME", "SURNAME", "POSITION", "SALARY"])

        for row, elem in enumerate(staff):
            for col, txt in enumerate(elem):
                table.setItem(row, col, QTableWidgetItem(str(txt)))
        table.verticalHeader().hide()
        table.move(50, 10)
        table.resize(400, 180)
        table.setColumnWidth(0, 1)
        table.setColumnWidth(1, 85)
        table.setColumnWidth(2, 85)
        table.setColumnWidth(4, 89)
        return table


    def swap(self):
        match self.wind_show:
            case 1: 
                self.wind_show = 0
                self.title = "Sanatorium [Service]"
            case 0: 
                self.wind_show = 1
                self.title = "Sanatorium [Staff]"
        self.initUI()
    

    def add_staff(self):
        dialog = New_staff(self)
        dialog.exec_()
    

    def remove_staff(self):
        dialog = Remove_staff(self)
        dialog.exec_()
    

    def add_service(self):
        dialog = New_service()
        dialog.exec_()
    

    def remove_service(self):
        dialog = Remove_service()
        dialog.exec_()



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Window()
    sys.exit(app.exec_())