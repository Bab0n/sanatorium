from PyQt5.QtWidgets import QPushButton, QLineEdit, QDialog, QLabel
import funcs


class New_staff(QDialog):
    def __init__(self, parent=None):
        super(New_staff, self).__init__(parent)
        self.initUI()
    
    def initUI(self):
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('New staff')
        self.name = self.input_space("Name", 50, 10)
        self.surname = self.input_space("Surname", 50, 40)
        self.position = self.input_space("Position", 50, 70)
        self.salary = self.input_space("Salary", 50, 100)
        self.close_button = QPushButton('Add', self)
        self.close_button.move(70, 140)
        self.close_button.clicked.connect(self.btnClosed)

    def input_space(self, text, posx, posy):
        space = QLineEdit(self)
        lbl = QLabel(text, self)
        lbl.move(posx-43, posy+3)
        space.move(posx, posy)
        return space

    def btnClosed(self):
        try:
            funcs.add_staff(self.name.text(), self.surname.text(), self.position.text(), int(self.salary.text()))
        except Exception:
            pass
        self.close()


class Remove_staff(QDialog):
    def __init__(self, parent=None):
        super(Remove_staff, self).__init__(parent)
        self.initUI()
    
    def initUI(self):
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('Staff remover')
        self.space = QLineEdit(self)
        self.lbl = QLabel('ID', self)
        self.lbl.move(50-20, 10+3)
        self.space.move(50, 10)
        self.close_button = QPushButton('Remove', self)
        self.close_button.move(50, 35)
        self.close_button.clicked.connect(self.btnClosed)

    def btnClosed(self):
        try:
            funcs.remove_staff(int(self.space.text()))
        except Exception:
            pass
        self.close()


class New_service(QDialog):
    def __init__(self, parent=None):
        super(New_service, self).__init__(parent)
        self.initUI()
    

    def initUI(self):
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('New service')
        self.title = self.input_space("Title", 50, 10)
        self.price = self.input_space("Price", 50, 40)
        self.close_button = QPushButton('Add', self)
        self.close_button.move(70, 70)
        self.close_button.clicked.connect(self.btnClosed)


    def input_space(self, text, posx, posy):
        space = QLineEdit(self)
        lbl = QLabel(text, self)
        lbl.move(posx-43, posy+3)
        space.move(posx, posy)
        return space


    def btnClosed(self):
        try:
            funcs.add_service(self.title.text(), int(self.price.text()))
        except Exception:
            pass
        self.close()


class Remove_service(QDialog):
    def __init__(self, parent=None):
        super(Remove_service, self).__init__(parent)
        self.initUI()
    

    def initUI(self):
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('Service remover')
        self.space = QLineEdit(self)
        self.lbl = QLabel('ID', self)
        self.lbl.move(50-20, 10+3)
        self.space.move(50, 10)
        self.close_button = QPushButton('Remove', self)
        self.close_button.move(50, 35)
        self.close_button.clicked.connect(self.btnClosed)


    def btnClosed(self):
        try:
            funcs.remove_service(int(self.space.text()))
        except Exception:
            pass
        self.close()